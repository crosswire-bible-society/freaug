#!/bin/bash
sed -ri 's/  / /g' *.usfm
sed -ri 's/(Id\.)([0-9])/\1 \2/g' *.usfm
sed -ri 's/(xt |ft |rq |; )Act,/\1\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Actes,/\1Act\./g' *.usfm

sed -ri 's/(xt |ft |rq |; )Aggée,/\1Agg\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Aggé,/\1Agg\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Amos,/\1Amo\./g' *.usfm

sed -ri 's/(xt |ft |rq |; )Apoc,/\1Apo\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Apoc\./\1Apo\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Baruch,/\1Bar\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Cant\,/\1Can\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Cant\./\1Can\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Colos,/\1Col\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Colos\./\1\Col\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Coloss,/\1\Col\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Coloss\./\1\Col\./g' *.usfm


sed -ri 's/(xt |ft |rq |; )Dan,/\1Dan\./g' *.usfm

sed -ri 's/(xt |ft |rq |; )Deut,/\1Deut\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Deut\./\1Deu\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Eccl,/\1Ecc\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Eccl\./\1Ecc\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Eccle\./\1Ecc\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Eccles\./\1Ecc\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Ecclés\./\1Ecc\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Eccli,/\1Sir\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Eccli\./\1Sir\./g' *.usfm

sed -ri 's/(xt |ft |rq |; )Ephés,/\1Eph\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Ephés\./\1Eph\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Éphés\./\1Eph\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Ephès\./\1Eph\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Éphes\./\1Eph\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Esdras,/\1Esd\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Esdr\./\1Esd\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Esdr,/\1Esd\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Esth,/\1Est\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Esth\./\1Est\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Exod,/\1Exo\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Exod\./\1Exo\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Exode,/\1Exo\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Ex\./\1Exo\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Ex,/\1Exo\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Ézéch\./\1Eze\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )(Ezéch\.|Ez\.)/\1Eze\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Ezéch,/\1Eze\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Ga\./\1Gal\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Gal,/\1Gal\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Galat\./\1Gal\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Gen,/\1Gen\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Gênés,/\1Gen\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Gênés\./\1Gen\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Habac\./\1Hab\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )(Héb,|Héb\.)/\1Heb\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Hebr\./\1Heb\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Hébr\./\1Heb\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Ib\./\1Id\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Ibid\./\1Id\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Id,/\1Id\./g' *.usfm

sed -ri 's/(xt |ft |rq |; )Idi\./\1Id\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Is\./\1Isa\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Isa,/\1Isa\./g' *.usfm

sed -ri 's/(xt |ft |rq |; )Isaï\./\1Isa\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Isaïe,/\1Isa\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Is\./\1Isa\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Jacob,/\1\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Jacques,/\1Jac\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )(Jacq\.|Jc\.)/\1Jac\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Jean,/\1Jn\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Jean\./\1Jn\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Joan\./\1Jn\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Jérém\./\1Jer\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Job,/\1Job\./g' *.usfm

sed -ri 's/(xt |ft |rq |; )Joel,/\1Jol\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Joël,/\1Jol\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Jonas,/\1Jon\./g' *.usfm

sed -ri 's/(xt |ft |rq |; )Josué,/\1Jos\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Judit\./\1Jug\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Judit,/\1Jug\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Juges,/\1Jug\./g' *.usfm
#sed -ri 's/(xt |ft |rq |; )ld\./\1\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Lévit\./\1Lev\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Lév\./\1Lev\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Luc,/\1Luc\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Lc\./\1Luc\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Malach\./\1Mal\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Marc,/\1Mrc\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Marc\./\1Mrc\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Mc\./\1Mrc\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Math\./\1Mat\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Matt,/\1Mat\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Matt\./\1Mat\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Matth,/\1Mat\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Matth\./\1Mat\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Mt\./\1Mat\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Mich\./\1Mic\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Naum\./\1Nah\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Nomb\./\1Nom\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Nombres,/\1Nom\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Num,/\1Nom\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Num\./\1Nom\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Numb\./\1Nom\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Nb\./\1Nom\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Osée,/\1Ose\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Osé,/\1Ose\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Os\./\1Ose\./g' *.usfm
#sed -ri 's/(xt |ft |rq |; )P\./\1\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )2 Paralip\./\12Ch\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Phil\./\1Phi\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Philip\./\1Phi\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Philipp,/\1Phi\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Philipp\./\1Phi\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Phïlipp\./\1Phi\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Pier\./\1Pi\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Prov\./\1Pro\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Ps,/\1Psa\./g' *.usfm

sed -ri 's/(xt |ft |rq |; )Ps\./\1Psa\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Psal\./\1Psa\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Psalm\./\1Psa\./g' *.usfm
#sed -ri 's/(xt |ft |rq |; )Roi,/\1\./g' *.usfm
#sed -ri 's/(xt |ft |rq |; )Rois,/\1\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )(Rom,|Rom\.)/\1Rom\./g' *.usfm

sed -ri 's/(xt |ft |rq |; )Sag,/\1Sag\./g' *.usfm


sed -ri 's/(xt |ft |rq |; )Sophon\./\1Sop\./g' *.usfm
#sed -ri 's/(xt |ft |rq |; )Thren\./\1\./g' *.usfm
#ed -ri 's/(xt |ft |rq |; )Tim\./\1\./g' *.usfm

sed -ri 's/(xt |ft |rq |; )Tite,/\1Tit\./g' *.usfm

sed -ri 's/(xt |ft |rq |; )Tobie,/\1Tob\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )Zach\./\1Zac\./g' *.usfm


sed -ri 's/(xt |ft |rq |; )1 Co,/\11Co\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Cor,/\11Co\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Cor\./\11Co\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Esdr\./\11Es\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Joan,/\11Jn\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Jn\./\11Jn\./g' *.usfm
#sed -ri 's/(xt |ft |rq |; )1 Matth\./\1\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Par\./\11Ch\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Paral\./\11Ch\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Paralip\./\11Ch\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Chr\./\11Ch\./g' *.usfm
#sed -ri 's/(xt |ft |rq |; )1 Philxt|ft |rq |; )1 Phil\./\1\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Petr\./\11Pi\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Piere,/\11Pi\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Piere /\11Pi\. /g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Pierre,/\11Pi\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Pi\./\11Pi\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Rois,|I Reg\./\11Sa\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Rois /\11Sa\. /g' *.usfm
sed -ri 's/(xt |ft |rq |; )I Thess\./\11Th./g' *.usfm
sed -ri 's/(xt |ft |rq |; )I Thessal\./\11 Th\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )(I Thes\.|1 Thes\.|1 Thess\.)/\11Th\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )I Thes\./\11Th\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Tim,/\11Ti\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Tim\./\11Ti\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )I Tim,/\11Ti\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )I Tim\./\11i\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 Tim\./\11 Ti\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )I Tm\./\1Ti\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )II Cor,/\12Co\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )2 Cor\./\12Co\./g' *.usfm
#sed -ri 's/(xt |ft |rq |; )2 Gor\\./\1\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )(II Joan,|2 Jn\.)/\12Jn\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )2 Macch\./\12Ma\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )2 Macchab\./\12Ma\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )2 Machab\./\12Ma\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )2 Mac\./\12Ma\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )II Pier\./\12Pi\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )(2 Pierre,|2 Pi\.)/\12Pi\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )II Reg,/\12Sa\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )2 Rois\./\12Sa\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )II Thess\./\12Th\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )(2 Thess\.|2 Thes\.)/\12 Th\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )II Tim,/\12Ti\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )2 Tim,/\12Ti\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )2 Tim\./\12Ti\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )III Reg,/\11Ro\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )3 Rois\./\11Ro\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )3 Rois,/\11Ro\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )IV Reg,/\12Ro\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )4 Reg\./\12Ro\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )2 R\./\12Ro\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )1 R\./\11Ro\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )(1 Sa\.|1 Sam\.)/\11Sa\./g' *.usfm
sed -ri 's/(xt |ft |rq |; )(2 Sa\.|2 Sam\.)/\12Sa\./g' *.usfm
sed -ri 's/\.\\rg\*/\\rg\*\./g' *.usfm
sed -ri 's/\. \\rg\*/\\rg\*\./g' *.usfm
sed -ri '/^$/d' *.usfm

#ajout espace entre \it et le texte qui le précède
sed -ri 's/([a-z])\\it,/\1, \\it /g;s/([a-z])\\it /\1 \\it /g' *.usfm

sed -ri "s/ 1'(É)/ l’\1/g;s/ 1'([a-z])/ l’\1/g" *.usfm
sed -ri 's/,» / », /g;s/,, /, /g;s/,\. ([a-z])/, \1/g;s/,; ([a-z])/, \1/g;s/\., ([a-z])/, \1/g;s/\.: «/ : «/g;s/\.; ([a-z])/ ; \1/g;s/, » / », /g;' *.usfm
sed -ri 's/([a-z])\\it,/\1, \\it /g;s/([a-z])\\it /\1 \\it /g' *.usfm
##Suppression des numéros de page
sed -ri 's/ \([0-9]*\) / /g;s/ \([0-9]*\)$//g;s/  / /g' *.usfm

sed -ri 's/Evangile/Évangile/g;s/Eglise/Église/g;s/Evangéliste/Évangéliste/g;s/Elie/Élie/g;s/au dessus/au-dessus/g;s/\. A /\. À /g;s/au dehors/au-dehors/g;s/au delà/au-delà/g;s/Ecriture/Écriture/g' *.usfm




